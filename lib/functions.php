<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../pages/home.php';
	} 
	else {
		include __DIR__.'/../pages/' . $_GET['page'];
	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

function getUserData() {
	$userData = file_get_contents(__DIR__ . '/../data/user.json');
	$userData = json_decode($userData);

	// var_dump($userData);

	foreach($userData as $key => $value) {
		if(is_array($value)) {
			foreach($value as $value2) {
				echo "<p> year : " . $value2->year . "</p>";
				echo "<p> company : " . $value2->company . "</p>";
			}
		}
		else {
			echo "<p>" . $key . " : " . $value . "</p>";
		}
		
	}
}


?>